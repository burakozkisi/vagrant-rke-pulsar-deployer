#!/bin/sh
KUBECTLVERSION=${1:-1.21.9}
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update

sudo swapoff -a
sudo swapoff -a
sudo sed -i '/swap/d' /etc/fstab
sudo systemctl enable iscsid --now

sudo apt-get install -y apt-transport-https jq ca-certificates curl gnupg-agent software-properties-common
sudo apt-get install -y ntp ntpdate

sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt-get update
sudo apt-get install  -y kubectl=$KUBECTLVERSION-00
sudo apt-mark hold kubectl
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

sudo usermod -aG docker $USER
sudo usermod -aG docker vagrant
newgrp docker
