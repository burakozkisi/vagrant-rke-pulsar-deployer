#!/bin/bash -x
SUBNETCIDR=${1:-192.168.57.0/24}
sudo apt update
sudo apt install nfs-kernel-server -y
sudo mkdir -p /nfs-share
sudo chown -R nobody:nogroup /nfs-share
sudo chmod 777 /nfs-share
sudo echo "/nfs-share $SUBNETCIDR/24(rw,sync,no_root_squash,no_subtree_check)" > /etc/exports
sudo exportfs -a
sudo systemctl restart nfs-kernel-server