
/**
 * 
 */
package com.axon_networks.pulsar;

import org.apache.pulsar.functions.api.Context;
import org.apache.pulsar.functions.api.Function;

/**
 * @author armangurkan
 *
 */
public class secondfunction implements Function<String, String> {
  @Override
  public String process(String input, Context context) {  
    String output = String.format("%s And I am doing great!!!", input);
    return output;
  }
}
