# Solution


- ./
- ├── helm
- ├── functions
- ├── README.md
- ├── .readme
- ├── scripts
- └── vagrant


## [Prepare Environment](./scripts)

This folder contains comman scripts that can be used to prepare build and dev environments.

* Take root privileges

```bash
sudo su
```

* Make scripts executable in the repository
  [prepare_env.sh](./scripts/prepare_env.sh)

```bash
chmod +x ./scripts/prepare_env.sh
```

* Prepare your build or dev env
  [ubuntu_build_env.sh](./scripts/ubuntu_build_env.sh)

```bash
.scripts/ubuntu_build_env.sh
```

* Don't forget to release root privileges

```bash
exit
```

### Directory Structure for common scripts


- ./
- ├── [scripts](./scripts)
- │   After infrastructure ready, we will use this script to automatically retrieve config-local-rke under $HOME/.kube/
- ├── [configure_kubeconfig.sh](./scripts/configure_kubeconfig.sh)
- │   Automatically installs rancher apps by providing app-key app-release-name app-namespace (Optional)
- ├── [install_rancher_app.sh](./scripts/install_rancher_app.sh)
- │   Installs rancher cli under /usr/local/bin directory (Optional)
- ├── [install_rancher_cli.sh](./scripts/install_rancher_cli.sh)
- │   Gives necessary permissions to all scripts under this repository (root priviliges)
- ├── [prepare_env.sh](./scripts/prepare_env.sh)
- │   Can be used while developing Vagrant infrastructure to quickly spinup machines (Optional-Unstable)
- ├── [rapid_spinup_machines.sh](./scripts/rapid_spinup_machines.sh)
- │   Installs all necessary binaries toolsets to machine (root priviliges)
- └── [ubuntu_build_env.sh](./scripts/ubuntu_build_env.sh)


## [SpinUp Rancher RKE with Vagrant](./vagrant)

This folder includes necessary IAC provisioning scripts for spinning up ubuntu 20.04 cloud image with rancher and rke kubernetes setup

* [config.yaml](./vagrant/config.yaml) includes necessary configuration like

  * memory
  * cpu
  * count of ctrlr and worker node
  * role flags for machines
  * static ip assignments
  * base image and version across the machines

- Influenced by rancher quickstart [rancher-vagrant-quickstart](https://github.com/rancher/quickstart/tree/master/vagrant)

  * Improved config yaml to suport spinup more machines
  * Changed base images rancherOs to ubuntu 20.04 cloud image for better supportfor persistence with longhorn
  * Add role flags to machines

### Directory structure for vagrant


- └── [vagrant](./vagrant/)
- |   - Config yaml for configuring HA Rke K8s cluster (Tested up to 3-ctrlr and 3-worker nodes)
- ├── [config.yaml](./vagrant/config.yaml)
- |   - Vagrant provisioner scripts stays here
- ├── [scripts](./vagrant/scripts)
- │   │   - Automatically detect OS and install latest Docker into worker/ctrlr/rancher nodes
- │   ├── [configure_docker.sh](./vagrant/scripts/configure_docker.sh)
- │   │   - Install necessary environments for a nfs server if needed (optional vagrant provisioner commented out)
- │   ├── [configure_nfs_server.sh](./vagrant/scripts/configure_nfs_server.sh)
- |   |   - Install necessary environments to support rancher rke environment
- │   ├── [configure_prerequisities.sh](./vagrant/scripts/configure_prerequisities.sh)
- |   |   - Parametric provisioner used for ctrlr and worker nodes
- │   ├── [configure_rancher_node.sh](./vagrant/scripts/configure_rancher_node.sh)
- |   |   - Spinnup rancher server with RKE k8s and create new cluster alive before other nodes created
- │   └── [configure_rancher_server.sh](./vagrant/scripts/configure_rancher_server.sh)
- |   - This python script helps us for preparing ansible host file (Optional)
- ├── [vagrant2inventory.py](./vagrant/vagrant2inventory.py)
- |   - Core vagrant file to bring Vms alive check [config.yaml](./vagrant/config.yaml)
- ├── [Vagrantfile](./vagrant/Vagrantfile)
- |   - Reboot blugin can be used by unsupported guests (Optional)
- └── [vagrant-provision-reboot-plugin.rb](./vagrant/vagrant-provision-reboot-plugin.rb)


### Bring up our Vm infrastructure

* 1 Rancher Server v 2.6.3
* 1 Ctrlr node with "--etcd --controller" roles
* 3 Worker node with "--worker" roles
* Prepare necessary beverages be patient, vagrant and scripts take care of this process (っ＾▿＾)۶🍸🌟🍺٩(˘◡˘ )
* Takes up to 30 mins depending on the host system resources ¯\_( ͡❛ ͜ʖ ͡❛)_/¯

```bash
cd ./vagrant
vagrant up
```

* Some vagrant box images may cause problem, The spinup process is resilient with ubuntu focal 20.04 cloud image,
* If you change the default configurations you can copy config.yaml and rename to local-config.yaml
* Vagrant will use local-config.yaml if exist
* For trouble shooting you can ssh into machines using cmd:

```bash
vagrant ssh rancher-srv-01 # ctrlr-node-01,worker-node-01 and so on
```

* After Rancher server alive you can check our Rancher server accesing [Rancher-Server](https://192.128.57.101)
* Proceed to website

![Proceed](.readme/proceed.png)

* Login with local user account
* Local user account name is `admin`
* Local user password is written in [config.yaml admin_password field](./vagrant/config.yaml)

![Login](.readme/login.png)

* Our cluster is ready

![Home](.readme/rancher_home.png)

* Cluster detail

![Cluster](.readme/cluster.png)

* Retrive admin user kubeconfig with this script.

* This script downloads admin kubeconfig under $HOME/.kube/ as config_local_rke
* Adds Into KUBECONFIG env values.
* This is not persistent between shell environments

[configure_kubeconfig.sh](./scripts/configure_kubeconfig.sh)

```bash
cd ../
./scrips/configure_kubeconfig.sh
```

* When needed scripts under this repo automatically export this

```bash
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/config-local-rke
```

## [Installing Helm Charts](./helm/)

* Rancher's monitoring, logging and longhorn charts coppied under `./helm/charts` this directory
* Some of the charts values are tailored depending on the requirements.
* If chart has values-local.yaml this file used for  overriding default values
* Rancher's monitoring, logging and longhorn charts copied into this directory
* To extend default dashboards particularly for Grafana

* Redis and Pulsar dashboard templates added can be seen below:
* [Pulsar Dashboard json](helm/charts/rancher-monitoring/files/rancher/redis-pulsar/pulsar.json)
* [Redis Dashboard json](helm/charts/rancher-monitoring/files/rancher/redis-pulsar/redis.json)
* [Custom Rancher Dashboard AddOn yaml](helm/charts/rancher-monitoring/templates/rancher-monitoring/dashboards/rancher/axon-redis-pulsar.yaml)
* For this dashboards you only need to pass `--set  grafana.defaultDashboardsEnabled: true` (Default is true)

### Directory Structure for helm

- └── helm
- |   Place for the charts that needs modification resides here
- ├── charts
- |   |   - Function mesh operator chart [info](https://streamnative.io/en/blog/release/2021-05-03-function-mesh-open-source/)
- │   ├── function-mesh-operator
- |   |   - Wellknow storage/persistence operator by writers of rancher [](https://longhorn.io/docs/1.2.3/what-is-longhorn/)
- │   ├── longhorn
- |   |   - longhorn crd helm template support deletion of crds (Helm doesn't support)
- │   ├── longhorn-crd
- |   |   - Nfs subdir provisioner chart (optional) [info](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner)
- │   ├── nfs-subdir-provisioner
- |   |   - Pulsar helm chart for preparing pulsar crds and installing pulsar
- │   ├── pulsar-helm-chart
- |   |   - Rancher logging chart
- │   ├── rancher-logging
- │   ├── rancher-logging-crd
- |   |   - Rancher monitoring chart
- │   ├── rancher-monitoring
- │   └── rancher-monitoring-crd
- |   - Delete pulsar and crds from cluster
- ├── [delete_pulsar.sh](helm/delete_pulsar.sh)
- |   - Installs function mesh operator
- ├── [install_function_mesh_operator.sh](helm/install_function_mesh_operator.sh)
- |   - Installs nfs subdir provisioner (optional)
- ├── [install_nfs_subdir_provisioner.sh](helm/install_nfs_subdir_provisioner.sh)
- |   - Installs pulsar helm chart
- ├── [install_pulsar.sh](helm/install_pulsar.sh)
- |   - Installs rancher monitoring-logging-longhorn
- ├── [install_longhorn.sh](helm/install_longhorn.sh)
- |   - Installs rancher monitoring-logging-longhorn
- ├── [install_rancher_monitoring.sh](helm/install_rancher_monitoring.sh)
- |   - Installs rancher monitoring-logging-longhorn
- ├── [install_rancher_logging.sh](helm/install_rancher_logging.sh)
- |   - Installs redis into cluster
- └── [install_redis.sh](helm/install_redis.sh)


### Install charts into our local-rke cluster

* Before installation you can check installed application page in Rancher UI (Empty)

![Installed Apps](.readme/before_installing_helm_charts.png)

* Installing operations should be sequenced as follow;

  * Installation of ranchers longhorn, monitoring and logging
  * Installation of pulsar
  * Installation of redis
  * Installation of function mesh operator

```bash
cd ./helm
./install_longhorn.sh # ~ tooks 9m
./install_rancher_monitoring.sh # ~ tooks 4m
./install_rancher_logging.sh # ~ tooks 4m
./install_pulsar.sh # ~ tooks 4m
./install_redis.sh # ~ tooks 4m
./install_function_mesh_operator.sh # ~ tooks 1m
```

* Installed Charts
![apps](.readme/after_installing_helm_charts.png)
* Cluster dashboard
![cluster-dashboard](.readme/cluster-dashboard.png)
* Longhorn dashboard
![Longhorn](.readme/longhorn-dashboard.png)
* Rancher monitoring
![Rancher monitoring](.readme/rancher-monitoring.png)
* Grafana/Pulsar
![Pulsar](.readme/pulsar_dashboard.png)
* Grafana/Redis
![Redis](.readme/redis_dashboard.png)

## [Functions](./functions)

* [README.md](./functions)

### Directory structure for pulsar functions


- .functions
- ├── axon_pulsar_functions
- │   ├── axon_edge_functions
- │   └── pom.xml
- ├── Dockerfile
- ├── function-mesh.yaml
- ├── Makefile
- ├── pom.xml
- ├── pulsar-io-redis-2.9.1.nar
- ├── README.md
- └── YAReadme.md


### Packaging

```bash
cd ./functions
mvn package
```

### Docker

```bash
docker build -t registry.gitlab.com/burakozkisi/vagrant-rke-pulsar-deployer/functions .
docker push -t registry.gitlab.com/burakozkisi/vagrant-rke-pulsar-deployer/functions
docker push registry.gitlab.com/burakozkisi/vagrant-rke-pulsar-deployer/functions
```

### Apply function-mesh crd

```bash
kubectl apply -f function-mesh.yaml
```
