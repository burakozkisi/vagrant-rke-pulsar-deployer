
#!/bin/bash
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/config-local-rke
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd $SCRIPTPATH/charts/pulsar-helm-chart

helm uninstall pulsar --namespace pulsar 
chmod +x ./scripts/pulsar/*
./scripts/pulsar/cleanup_helm_release.sh -d \
     --namespace pulsar \
     --release pulsar 
kubectl get pods -n pulsar

cd $OLDPWD
