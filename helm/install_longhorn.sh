#!/bin/bash
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/config-local-rke
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
helm upgrade --install --create-namespace --timeout=10m0s --wait longhorn-crd -n longhorn-system $SCRIPTPATH/charts/longhorn-crd 
helm upgrade --install --create-namespace --timeout=10m0s --wait longhorn -n longhorn-system $SCRIPTPATH/charts/longhorn -f $SCRIPTPATH/charts/longhorn/values-local.yaml
