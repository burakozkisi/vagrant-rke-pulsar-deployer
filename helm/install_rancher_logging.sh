#!/bin/bash
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/config-local-rke
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
helm upgrade --install --create-namespace --timeout=10m0s --wait rancher-logging-crd -n cattle-logging-system $SCRIPTPATH/charts/rancher-logging-crd 
helm upgrade --install --create-namespace --timeout=10m0s --wait rancher-logging -n cattle-logging-system $SCRIPTPATH/charts/rancher-logging  -f $SCRIPTPATH/charts/rancher-logging/values-local.yaml
