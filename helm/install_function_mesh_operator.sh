#!/bin/bash
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/config-local-rke
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd $SCRIPTPATH/charts/function-mesh
curl -sSL https://github.com/streamnative/function-mesh/releases/download/v0.1.8/crd.yaml | kubectl create -f -
kubectl create ns function-mesh
helm install function-mesh --values charts/function-mesh-operator/values.yaml charts/function-mesh-operator --namespace=function-mesh
kubectl get pods --namespace function-mesh -l app.kubernetes.io/instance=function-mesh
cd $OLDPWD
