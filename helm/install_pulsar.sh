
#!/bin/bash
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/config-local-rke
helm repo add apache https://pulsar.apache.org/charts
helm repo update apache

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

cd $SCRIPTPATH/charts/pulsar-helm-chart

chmod +x ./scripts/pulsar/*
./scripts/pulsar/prepare_helm_release.sh \
    -n pulsar \
    -k pulsar \
    -c

helm upgrade --install --wait --timeout=10m0s \
    --values ./examples/values-one-node.yaml \
    --namespace pulsar \
    pulsar apache/pulsar

kubectl get pods -n pulsar

cd $OLDPWD

# kubectl get services -n pulsar
# kubectl exec -it -n pulsar pulsar-mini-toolset-0 -- /bin/bash
# bin/pulsar-admin tenants create apache
# bin/pulsar-admin tenants list
# bin/pulsar-admin namespaces create apache/pulsar
# bin/pulsar-admin namespaces list apache
# bin/pulsar-admin topics create-partitioned-topic apache/pulsar/test-topic -p 4
# bin/pulsar-admin topics list-partitioned-topics apache/pulsar

# # download pulsar
# curl https://pulsar.apache.org/en/download/

# tar -xf <file-name>.tar.gz

# export PULSAR_HOME=$(pwd)
# bin/pulsar-client consume -s sub apache/pulsar/test-topic  -n 0

# bin/pulsar-client produce apache/pulsar/test-topic  -m "---------hello apache pulsar-------" -n 10
# ##DNS part
# docker pull coredns/coredns
