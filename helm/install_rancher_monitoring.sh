#!/bin/bash
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/config-local-rke
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
helm upgrade --install --create-namespace --timeout=10m0s --wait rancher-monitoring-crd -n cattle-monitoring-system $SCRIPTPATH/charts/rancher-monitoring-crd 
helm upgrade --install --create-namespace --timeout=10m0s --wait rancher-monitoring -n cattle-monitoring-system $SCRIPTPATH/charts/rancher-monitoring  -f $SCRIPTPATH/charts/rancher-monitoring/values-local.yaml
