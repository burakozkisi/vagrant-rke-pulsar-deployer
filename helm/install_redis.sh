#!/bin/bash
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/config-local-rke
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update bitnami
helm upgrade --install redis --namespace redis --create-namespace  --wait --timeout=10m0s\
    --set global.storageClass=longhorn \
    --set global.redis.password=redispassword \
    --set master.persistence.size=2Gi \
    --set replica.persistence.size=2Gi \
    --set metrics.enabled=true \
    --set metrics.serviceMonitor.enabled=true \
    --set metrics.serviceMonitor.namespace=redis \
    bitnami/redis

# To get your password run:

#     export REDIS_PASSWORD=$(kubectl get secret --namespace redis redis -o jsonpath="{.data.redis-password}" | base64 --decode)

# To connect to your Redis&trade; server:

# 1. Run a Redis&trade; pod that you can use as a client:

#    kubectl run --namespace redis redis-client --restart='Never'  --env REDIS_PASSWORD=$REDIS_PASSWORD  --image docker.io/bitnami/redis:6.2.6-debian-10-r120 --command -- sleep infinity

#    Use the following command to attach to the pod:

#    kubectl exec --tty -i redis-client \
#    --namespace redis -- bash

# 2. Connect using the Redis&trade; CLI:
#    REDISCLI_AUTH="$REDIS_PASSWORD" redis-cli -h redis-master
#    REDISCLI_AUTH="$REDIS_PASSWORD" redis-cli -h redis-replicas

# To connect to your database from outside the cluster execute the following commands:

#     kubectl port-forward --namespace redis svc/redis-master : &
#     REDISCLI_AUTH="$REDIS_PASSWORD" redis-cli -h 127.0.0.1 -p