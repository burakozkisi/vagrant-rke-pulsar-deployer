#!/bin/bash
NFSSERVERIP=${1:-192.168.56.130}
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/config-local-rke
helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
helm repo update nfs-subdir-external-provisioner
helm upgrade --wait --install nfs-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
    -n nfs-share --create-namespace\
    --set nfs.server=$NFSSERVERIP \
    --set nfs.path=/nfs-share \
    --set storageClass.defaultClass=true