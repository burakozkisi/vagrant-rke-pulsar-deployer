
APPKEY=$1
APPNAME=$2
APPNAMESPACE=$3

RANCHERENDPOINT=${4:-https://192.168.57.101}
# Username, password and realname of the user
PASSWORD=${5:-123qwe123qwe123qwe123qwe}
# The name of the cluster where the user needs to be added
CLUSTERNAME=${6:-local-rke}
RANCHERUSERNAME=${7:-admin}


ADMINBEARERTOKEN=$(curl -s $RANCHERENDPOINT'/v3-public/localProviders/local?action=login' -H 'content-type: application/json' --data-binary '{"username":"'$RANCHERUSERNAME'","password":"'$PASSWORD'"}' --insecure  | jq -r .token)
# Get clusterid from name
CLUSTERID=$(curl -s -u $ADMINBEARERTOKEN $RANCHERENDPOINT'/v3/clusters?name='$CLUSTERNAME --insecure | jq -r .data[].id)

# Generate and save kubeconfig
echo 2 | rancher login --token $ADMINBEARERTOKEN --skip-verify  $RANCHERENDPOINT
LOCALRKECLUSTERID=$(rancher cluster ls | grep $CLUSTERNAME | awk '{print $2}')
echo $LOCALRKECLUSTERID
SYSTEMPROJECT=$(rancher project $LOCALRKECLUSTERID | grep System | awk '{print $1}')
echo $SYSTEMPROJECT
DEFAULTPROJECT=$(rancher project $LOCALRKECLUSTERID | grep Default | awk '{print $1}')
echo $DEFAULTPROJECT
rancher login --token $ADMINBEARERTOKEN --skip-verify --context $SYSTEMPROJECT  $RANCHERENDPOINT 
APPID=$(rancher apps lt | grep $APPKEY | awk '{print $1}')
echo $APPID
rancher app install  $APPID $APPNAME -n $APPNAMESPACE --no-prompt   

