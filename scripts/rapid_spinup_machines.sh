#!/bin/bash
pids=""
RESULT=0

vagrant up rancher-srv-01 ctrlr-node-01 > /dev/null 2>&1 &
pids="$pids $!"

for pid in $pids; do
    wait $pid || let "RESULT=1"
done

if [ "$RESULT" == "1" ];
    then
       exit 1
fi

pids=""
RESULT=0

vagrant up worker-node-01 > /dev/null 2>&1 &
pids="$pids $!"
vagrant up worker-node-02 > /dev/null 2>&1 &
pids="$pids $!"
vagrant up worker-node-03 > /dev/null 2>&1 &
pids="$pids $!"

for pid in $pids; do
    wait $pid || let "RESULT=1"
done

if [ "$RESULT" == "1" ];
    then
       exit 1
fi