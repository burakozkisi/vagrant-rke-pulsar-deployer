#!/bin/bash
RANCHERENDPOINT=${1:-https://192.168.57.101/v3}
# Username, password and realname of the user
PASSWORD=${2:-123qwe123qwe123qwe123qwe}
# The name of the cluster where the user needs to be added
CLUSTERNAME=${3:-local-rke}
RANCHERUSER=${4:-admin}

ADMINBEARERTOKEN=$(curl -s $RANCHERENDPOINT'-public/localProviders/local?action=login' -H 'content-type: application/json' --data-binary '{"username":"'$RANCHERUSER'","password":"'$PASSWORD'"}' --insecure  | jq -r '.token')
# Get clusterid from name
CLUSTERID=$(curl -s -u $ADMINBEARERTOKEN $RANCHERENDPOINT'/clusters?name='$CLUSTERNAME --insecure | jq -r '.data[].id')

rm -f $HOME/.kube/config-local-rke
mkdir -p $HOME/.kube
# Generate and save kubeconfig
curl -s -u $ADMINBEARERTOKEN $RANCHERENDPOINT'/clusters/'$CLUSTERID'?action=generateKubeconfig' -X POST -H 'content-type: application/json' --insecure | jq -r '.config' > $HOME/.kube/config-local-rke

chmod 666 $HOME/.kube/config-local-rke
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/config-local-rke
kubectl version
kubectl get all -A
