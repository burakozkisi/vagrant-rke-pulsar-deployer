#!/bin/bash

# Download rancher and rancher-compose command line tools
wget -O rancher-cli.tar.gz https://github.com/rancher/cli/releases/download/v2.6.0/rancher-linux-amd64-v2.6.0.tar.gz

# extract the binaries from the tar archive
sudo tar -xzvf rancher-cli.tar.gz -C /usr/local/bin --strip-components=2

# Remove the archive
rm rancher-cli.tar.gz rancher-compose.tar.gz -f