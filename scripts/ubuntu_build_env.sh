#!/bin/bash
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi
#vim
echo "Installing vim..."
apt update
apt install -y vim
#kubectl
echo "Installing kubectl v1.21.9 and holding it..."
curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
apt-get install  -y kubectl=1.21.9-00 --allow-downgrade
apt-mark hold kubectl
#terraform
echo "Installing Terraform..."
curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com focal main"
# helm
echo "Installing Helm..."
curl https://baltocdn.com/helm/signing.asc | apt-key add -
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list
apt-get update
apt-get install -y helm
#Vbox and Vagrant
echo "Installing Vagrant and Virtualbox"
apt-get install -y virtualbox vagrant
#ansible
echo "Installing Ansible"
apt install -y ansible
#git git-lfs
echo "Installing Git and Git-Lfs"
apt install -y git git-lfs
#python3
echo "Installing Python3"
apt install -y python3
apt install -y python-is-python3
#default java jre jdk
echo "Installing Java JRE JDk MAVEN"
apt install -y default-jre
apt install -y default-jdk
apt install -y maven
#docker
echo "Installing Docker"
apt install -y  gnupg  lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu  impish  stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
apt update
apt install -y docker-ce docker-ce-cli containerd.io
#vagrant plugins
echo "Installing Vagrant Plugins"
vagrant plugin install vagrant-vbguest
vagrant plugin install vagrant-reload
vagrant plugin install vagrant-scp
#gitlab runner
curl -L --output /usr/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
chmod +x /usr/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
usermod -a -G sudo gitlab-runner
usermod -aG docker gitlab-runner
usermod -aG vboxusers gitlab-runner
echo "gitlab-runner ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers

gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token jzoUBe-ZzRFpbBoUxM64 \
  --executor shell \
  --shell bash \
  --name local-pc 
  
  
